from django.shortcuts import render
from django.http import HttpResponseRedirect,HttpResponse
from django.template import loader
from .forms import NameForm
from .models import Name

def index(request):
    return render(request, 'form/name.html')

def display(request):
    if request.method == 'POST':
        form=NameForm(request.POST)
        if form.is_valid():
            nameform=form.save(commit=False)
            nameform.name = request.POST['name']
            nameform.city = request.POST['city']
            nameform.number = request.POST['number']
            if request.POST['s_id']:
                obj = Name.objects.get(s_id=request.POST['s_id'])
                obj.name = request.POST['name']
                obj.city = request.POST['city']
                obj.number = request.POST['number']
                obj.save()
            else:
                nameform.save()
    query_results = Name.objects.all()
    return render(request, 'form/display.html', {'query_results': query_results})

def delete(request,id):
    Name.objects.filter(s_id=id).delete()
    query_results = Name.objects.all()
    return render(request, 'form/display.html', {'query_results': query_results})


def edit(request,id):
    #Name.objects.filter(s_id=id).delete()
    #query_results = Name.objects.all()
    obj = Name.objects.get(s_id = id)
    return render(request, 'form/name.html', {'name': obj.name, 'city':obj.city,'number':obj.number,'s_id':id})
    